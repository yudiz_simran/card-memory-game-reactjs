import logo from './logo.svg';
import './App.css';

/**************************React Packages***************************/
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { ToastContainer, Flip } from "react-toastify"



/********************************Css file*****************************/
import "react-toastify/dist/ReactToastify.css";

/**************************Components***************************/
import Login from './Components/Login/login';
import Room from './Components/Room/Room';

function App() {
  return (
    <div className="App">
      {/* <Login />/ */}
      
      <Router>
        <Switch>
           <Route exact path="/room" component={Room}></Route>
           <Route exact path="/" component={Login}></Route>
        </Switch>
      </Router>
      
      {/* Toastify notifications */}
      <ToastContainer
              position="top-right"
              transition={Flip}
              autoClose={3000}
              hideProgressBar={true}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              // pauseOnFocusLoss
              draggable
              pauseOnHover
         />
    </div>
  );
}

export default App;
