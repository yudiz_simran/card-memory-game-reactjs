import React, {Fragment, fragment} from 'react'

/****************React Packages***************/
import { IntlProvider } from 'react-intl'

import { LANG } from './languages'

import messages from './messages'

const Provider = ({children, lang = LANG.ENGLISH}) => (
    <IntlProvider
      lang={LANG}
      textComponent={Fragment}
      messages={messages[lang]}
    >    
    {children}
    </IntlProvider>
)

export default Provider

