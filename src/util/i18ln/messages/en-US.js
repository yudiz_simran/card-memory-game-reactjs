import { LANG } from "../languages";

export default {
    [LANG.ENGLISH]:{
        'Welcome':'Welcome',
        'Languages':'Languages',
        'Shuffle':'Shuffle',
        'Score':'Score'
    }
}