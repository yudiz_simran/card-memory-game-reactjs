import { LANG } from "../languages";

export default {
    [LANG.FRENCH]:{
        'Welcome':'Bienvenue',
        'Languages':'Langues',
        'Shuffle':'Mélanger',
        'Score':'But'
    }
}