import { LANG } from "../languages";

export default {
    [LANG.GERMAN]:{
        'Welcome':'Willkommen',
        'Languages':'Sprachen',
        'Shuffle':'Mischen',
        'Score':'Punktzahl'
    }
}