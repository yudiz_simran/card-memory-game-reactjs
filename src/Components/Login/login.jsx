import React, { Component } from 'react';

/*************************Css File**************************/
import '../../App.css'

/********************************React-Packages*****************************/
import {
  Link,
} from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";


/**********************Recat - Icons************************/
import { GiCardPickup } from "react-icons/gi";

/**********************Yup-Validation****************************************/
import * as Yup from 'yup';

/**********************Validation-Schema****************************************/
const ValidationSchema = Yup.object().shape({
  uname: Yup.string()
    .required('Username is required')
    .matches(/^(\S)/, 'This field cannot contain only blankspaces'),

});

class Login extends Component {

   constructor(props){
     super(props)
   }


   onHandleSubmit = (val) => {
      console.log("val",val)
      const {history} = this.props
      localStorage.setItem("Username",val.uname)
      history.push('/room')
      
   }


    render() {
        return (
            <>
              <section className="ftco-section">
                <div className="container">
                  <div className="row justify-content-center">
                </div>
                <div className="row justify-content-center login-width">
                  <div className="col-md-6 col-lg-5">
                    <div className="login-wrap p-4 p-md-5">
                        <div className="icon d-flex align-items-center justify-content-center">
                          <GiCardPickup className="icon-color"></GiCardPickup>
                        </div>
                        <h4 className="h4">Memory Card Game</h4>
                        <Formik
                            enableReinitialize
                            initialValues={{uname:''}}
                            validationSchema={ValidationSchema}
                            onSubmit={this.onHandleSubmit}
                            >
                          {({
                              values,
                              errors,
                              touched,
                              handleChange,
                              handleBlur,
                              handleSubmit,
                              submitCount,
                              setFieldValue,
                          }) => {
                              return (
                                      <>                    
                                        <form onSubmit={handleSubmit} >
                                        <div className="form-group">
                                                {/* <input type="text" name="uname" className="form-control rounded-left" placeholder="Username" /> */}
                                                <Field
                                                    name="uname"
                                                    label="Username"
                                                    autoComplete="given-name"
                                                    className="form-control rounded-left"
                                                    placeholder="Username..."
                                                />
                                            </div>
                                            {errors.uname && touched.uname ? (
                                                <div className="error">{errors.uname}</div>
                                            ) : null}

                                            <div className="form-group">
                                                <button type="submit" className="btn btn-primary rounded submit p-3 px-5">Get Started</button>
                                            </div>
                                        </form>
                            
                                        </>
                                      );
                                 }}
                         </Formik>
                      </div>
                    </div>
                  </div>
                </div>
            </section>
            </>
        );
    }
}

export default Login;
