import React, {useState} from 'react'

/************************React Icons**************************/
import { CgCloseR } from "react-icons/cg";

/********************************React-Packages*****************************/
import {
    Link,
    useHistory
  } from "react-router-dom";
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import { I18nProvider,LANG } from '../../util/i18ln'
import { FormattedMessage  } from 'react-intl'

import translate from '../../util/i18ln/translate';

/***********************React - Icons*************************/
import { BiWorld } from "react-icons/bi";

/*******************Components******************************/
import Main from '../Main';

function Room() {

    const history = useHistory(); 

    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [value, setValue] = useState(LANG.ENGLISH);

    const toggle = () => setDropdownOpen(prevState => !prevState);

    function handleClose(){
        localStorage.removeItem("Username")
        history.push('/')
    }

    function handleChange(event){
      let local = ''
      console.log("event",event.currentTarget.textContent);
      setValue(event.currentTarget.textContent)
      console.log("vlll",value);
     
    }

    return (
        <>
             {/* Navbar-Header */}
             <I18nProvider  lang={value}>
                <nav className="navbar navbar-light bg-light width header">
                  <span className="navbar-brand mb-0 h1"> <FormattedMessage id="Welcome" /> {localStorage.getItem("Username")}</span>
                  <Dropdown isOpen={dropdownOpen} toggle={toggle} className="ml-auto mx-4" >
                      <DropdownToggle caret className="btn btn-warning icon-color">
                         <FormattedMessage id="Languages" />
                      </DropdownToggle>
                      <DropdownMenu  className="dropdown-menu-right">
                        <DropdownItem 
                           onClick={() => setValue(LANG.ENGLISH)}>
                             ENGLISH
                         </DropdownItem>
                         <DropdownItem 
                            onClick={() => setValue(LANG.FRENCH)}>
                             FRENCH
                         </DropdownItem>
                         <DropdownItem 
                           onClick={() => setValue(LANG.GERMAN)}>
                             GERMAN
                         </DropdownItem>
                      </DropdownMenu>
                    </Dropdown>
                    
                  <button 
                     className="btn btn-sm btn-warning"
                     onClick={handleClose}
                     >
                         <CgCloseR className="icon-color"></CgCloseR>
                  </button>
                </nav>

             {/* Main Component to play game */}
               <Main />
            </I18nProvider>
        </>
    )
}

export default Room
