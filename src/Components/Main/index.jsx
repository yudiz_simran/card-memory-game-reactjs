import React, {useContext} from "react";

/*********************React-Packages***************************/
import { useEffect, useState, useRef } from "react";
import {
  Link,
  useHistory
} from "react-router-dom";

/************************Components*******************************/
import CardView from "../Cards-view/index";

/************************Styles file************************/
import "../../Main-view.scss";


import { apiToastError, apiToastSuccess } from "../../util/util";

import { FormattedMessage  } from 'react-intl'

/**********************************Global State********************************/
import { GlobalContext } from '../../Context/GlobalState';

function Main() {
  const history = useHistory(); 
   
 const {Cards} = useContext(GlobalContext);
  const [cards, setCards] = useState(() => Cards.concat(Cards));
  const [scores, setScores] = useState(0);
  const [openCards, setOpenCards] = useState([]);
  const [clearedCards, setClearedCards] = useState({});
  const [allCards, setAllCards] = useState(false);

  
  useEffect(() => {
   
    let timeout = null;
    if (openCards.length === 2) {
      timeout = setTimeout(evaluate, 400);
    }
    if(Object.keys(clearedCards).length === Cards.length){
      history.push('/')
      apiToastSuccess(`Total scores ${scores}`)
      apiToastSuccess("Game Completed")
    }
    // shuffleCards();
    return () => {
      clearTimeout(timeout);
    };

    
    
  },[openCards]);

  const EnableCards = () => {
    setAllCards(false);
  };

  const DisableCards = () => {
    setAllCards(true);
  };

  const RefVariable = useRef(null);

  const evaluate = () => {
    const [first, second] = openCards;
    EnableCards();
    if (cards[first].name === cards[second].name) {
      setClearedCards((prev) => ({ ...prev, [cards[first].name]: true }) );
      // setOpenCards((prev) => [...prev, index]);
      setScores((moves) => moves + 1);
      setOpenCards([])
      return;
    }
    RefVariable.current = setTimeout(() => {
      setOpenCards([]);
      
    }, 10);
  };

  const handleCardClick = (index) => {
    console.log("index",index);
    if (openCards.length === 1) {
      setOpenCards((prev) => [...prev, index]);
      DisableCards();
    } else {
      clearTimeout(RefVariable.current);
      setOpenCards([index]);
    }
    // console.log("open",openCards);
  };

  const shuffleCards = () => {
    var i = cards.length,
      j = 0,
      temp;

    while (i--) {
      j = Math.floor(Math.random() * (i + 1));

      temp = cards[i];
      cards[i] = cards[j];
      cards[j] = temp;
    }
    return cards;
  }

  const checkIsFlipped = (index) => {
    return openCards.includes(index);
  };

  const checkIsInactive = (card) => {
    
    return Boolean(clearedCards[card.name]);
  };

  return (
    <div>
      <div>
        <div className="App">
          
          <div className="container mt-4">
            {cards?.map((card, index) => {
              return (
                <CardView
                  key={index}
                  card={card}
                  index={index}
                  isDisabled={allCards}
                  isInactive={checkIsInactive(card)}
                  isFlipped={checkIsFlipped(index)}
                  onClick={handleCardClick}
                />
              );
            })}
            
          </div>
          <div className="col-lg-12 container">
                <button 
                  className="btn btn-primary score-button mt-4 ml-4"
                  onClick={shuffleCards}
                  >
                  <FormattedMessage id="Shuffle" />
                </button>
                <button className="btn btn-warning score-button mt-4 ml-4">
                <FormattedMessage id="Score" />   {scores}
                </button>
            </div>
        </div>
      </div>
    </div>
  );
}

export default Main;


