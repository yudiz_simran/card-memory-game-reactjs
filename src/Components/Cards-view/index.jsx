import React from "react";

/*********************React-PAckages***************************/
import classnames from "classnames";

/************************Question-Mark************************/
import mark from "../../assets/images/question-mark.png";

/************************Styles file************************/
import "../../Cards-view.scss";


const CardView = ({ onClick, card, index, isInactive, isFlipped, isDisabled }) => {
  const handleClick = () => !isFlipped && !isDisabled && onClick(index);
  return (
    <div
      className={classnames("card", {
        "is-flipped": isFlipped,
        "is-inactive": isInactive,
      })}
      onClick={handleClick}
    >
      <div className="card-face card-font-face">
        <img src={mark} alt="mark" />
      </div>
      <div className="card-face card-back-face">
        <img src={card.image} alt={card.name} />
      </div>
    </div>
  );
};


export default CardView;
